//
//  ExperienceViewController.swift
//  lab-2
//
//  Created by Nils Müller on 10.11.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import Foundation
import UIKit

class ExperienceViewController: UITableViewController {

    var experienceCollections = [ExperienceCollection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let photo1 = UIImage(systemName: "pencil")
        let exp1 = Experience(title: "Work 1", photo: photo1, time: "2013 - 2014", content: """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium aliquet tristique. Nullam tempus, velit ac imperdiet blandit, lorem ante convallis tellus, eu egestas dolor mauris a eros. Donec quis tellus malesuada, euismod ante euismod, consectetur nisi. Quisque fringilla dolor sem, id imperdiet turpis pulvinar et. Vestibulum condimentum, turpis sed hendrerit ornare, felis magna ullamcorper mi, et iaculis erat urna vitae libero. Fusce non justo eu sem condimentum viverra. Nulla purus odio, varius id mattis in, fringilla eu leo. Etiam pellentesque metus eu ipsum pharetra, ut pretium turpis convallis. Maecenas laoreet tellus eget mauris tristique, sed scelerisque libero pulvinar. Etiam cursus ante id dictum eleifend. Praesent bibendum tempor ullamcorper. Aenean aliquam in dui et dapibus. Nam id mollis velit.
""")
        
        let photo2 = UIImage(systemName: "trash")
        let exp2 = Experience(title: "Work 2", photo: photo2, time: "2014 - 2017", content: "lorem ipsum 2")
        
        let photo3 = UIImage(systemName: "folder")
        let exp3 = Experience(title: "Work 3", photo: photo3, time: "2017 - 2018", content: "lorem ipsum 3")
        
        let expCollection1 = ExperienceCollection(name: "Work", experiences: [exp1, exp2, exp3] )
        
        let photo4 = UIImage(systemName: "book")
        let exp4 = Experience(title: "Education", photo: photo4, time: "2018 - current", content: "lorem ipsum 1")
        
        let expCollection2 = ExperienceCollection(name: "Education", experiences: [exp4] )
        
        experienceCollections.append(expCollection1)
        experienceCollections.append(expCollection2)
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return experienceCollections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experienceCollections[section].experiences.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let experience = experienceCollections[indexPath.section].experiences[indexPath.row]
        
        cell.titleLabel.text = experience.title
        cell.photoImageView.image = experience.photo
        cell.timeLabel.text = experience.time
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ExperienceDetailViewController
        {
            let indexPath = tableView.indexPathForSelectedRow!
            vc.experience = experienceCollections[indexPath.section].experiences[indexPath.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return experienceCollections[section].name
    }
}
