//
//  Experience.swift
//  lab-2
//
//  Created by Nils Müller on 10.11.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import Foundation

import UIKit


class Experience {
    
    let title: String
    let photo: UIImage?
    let time: String
    let content: String
    
    
    init(title: String, photo: UIImage?, time: String, content: String) {
        self.title = title
        self.photo = photo
        self.time = time
        self.content = content
    }
}
