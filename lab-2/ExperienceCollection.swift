//
//  ExperienceCollection.swift
//  lab-2
//
//  Created by Nils Müller on 10.11.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import Foundation

import UIKit


class ExperienceCollection {
    
    let name: String
    let experiences: [Experience]
    
    
    init(name: String, experiences: [Experience]) {
        self.name = name
        self.experiences = experiences
    }
}
