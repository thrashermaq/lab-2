//
//  SkillsViewController.swift
//  lab-2
//
//  Created by Nils Müller on 10.11.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var animateView: UIView!
    
    var visible : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func animate(_ sender: Any) {
        UIView.animate(withDuration: 1) {
            if (self.visible) {
                self.animateView.alpha = 0
            } else {
                self.animateView.alpha = 1
            }
            self.visible = !self.visible
        }
    }
    
    @IBAction func exitButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
