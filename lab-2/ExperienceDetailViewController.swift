//
//  ExperienceDetailViewController.swift
//  lab-2
//
//  Created by Nils Müller on 10.11.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import Foundation

import UIKit

class ExperienceDetailViewController: UIViewController {

    var experience: Experience = Experience(title: "", photo: nil, time: "", content: "")
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        titleLabel?.text = experience.title
        timeLabel?.text = experience.time
        contentLabel?.text = experience.content
        photo?.image = experience.photo
        navigationItem.title = experience.title
    }


}
